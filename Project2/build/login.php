<!DOCTYPE html>
<html>
    <head>
        <title>BRAINSTER Library</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/a68a5718a6.js" crossorigin="anonymous"></script>
    </head>
    <body class="min-h-screen">

        <?php 
        
            if (isset($_GET['error'])) {
                echo "<div class='alert alert-danger'>" . $_GET['error'] . "</div>";
            }
        
        ?>
    
        <section class="md:w-4/5 m-auto p-4 container mx-auto">
            <div class="row items-center">
                <div class="col-6">
                    <h2 class="h2">Log in</h2>
                    <form action="login_script.php" method="POST">
                        <div class="form-group">
                            <label for="email" >Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                        </div> 

                        <div class="form-group">
                            <label for="password" >Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        </div>

                        <button class="btn btn-success">Log in</button>

                    </form>

                </div>
                <div class="col-6">
                    <h2 class="h2">Register</h2>
                    <form action="registration_script.php" method="POST">
                        <div class="form-group">
                            <label for="name" >Name</label>
                            <input type="name" name="name" id="name" class="form-control" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <label for="surname" >Sur Name</label>
                            <input type="surname" name="surname" id="surname" class="form-control" placeholder="Sur Name" required>
                        </div>
                        <div class="form-group">
                            <label for="email" >Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="password" >Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password" >Confirm Password</label>
                            <input type="confirm_password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Password" required>
                        </div>
                        <button class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>
        </section>

        <footer></footer>
        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>