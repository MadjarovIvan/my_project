<?php 

require_once __DIR__ . '/Database/Connection.php';
require_once __DIR__ . '/Books/Books.php';
require_once __DIR__ . '/Authors/Authors.php';
require_once __DIR__ . '/Categories/Categories.php';
require_once __DIR__ . '/Users/Users.php';

use Database\Connection as Connection;
use Books\Books as Books;
use Authors\Authors as Authors;
use Cathegory\Cathegory as Cathegory;
use Users\Users as Users;

$connectionObj = new Connection();
$connection = $connectionObj -> getPdo();

$books_sql = 'SELECT Books.*, authors.*, Category.category_title FROM Books INNER JOIN Authors on Books.author_id = Authors.id INNER JOIN Category on Books.category_id = Category.id';
$books_stmt = $connection -> prepare($books_sql);
$books_stmt -> execute();
$books = $books_stmt -> fetchAll(PDO::FETCH_ASSOC);

$categories_sql = 'SELECT * FROM Category';
$categories_stmt = $connection -> prepare($categories_sql);
$categories_stmt -> execute();
$categories = $categories_stmt -> fetchAll(PDO::FETCH_ASSOC);

$authors_sql = 'SELECT * FROM Authors';
$authors_stmt = $connection -> prepare($authors_sql);
$authors_stmt -> execute();
$authors = $authors_stmt -> fetchAll(PDO::FETCH_ASSOC);

$users_sql = 'SELECT Users.*, Roles.* FROM Users INNER JOIN Roles on Users.role_id = Roles.id';
$users_stmt = $connection -> prepare($users_sql);
$users_stmt -> execute();
$users = $users_stmt -> fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>BRAINSTER Library</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/a68a5718a6.js" crossorigin="anonymous"></script>
    </head>
    <body class="min-h-screen">
        <section class="md:w-4/5 m-auto p-4 text-center">
            <h3 class="h3">All Books</h3>
            <table class="text-center m-auto">
                <tr>
                    <th>Book Title</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Release Year</th>
                    <th>Number of pages</th>
                    <th>Edit</th>
                </tr>
                <?php 
                
                foreach ($books as $book) {
                    echo '
                    <tr>
                        <td>'.$book['book_title'].'</td>
                        <td>'.$book['author_name'], $book['author_surname'].'</td>
                        <td>'.$book['category_title'].'</td>
                        <td>'.$book['release_year'].'</td>
                        <td>'.$book['number_of_pages'].'</td>
                        <td><a href="edit_book.php?id='.$book['id'].'">Edit</a></td>
                    </tr>
                    ';
                }
                
                ?>

            </table>

            <div class="">
                <a href="add_book.php"> + Add Book </a>
            </div>


        </section>

        <section class="md:w-4/5 m-auto p-4 text-center">
            <div class="grid grid-cols-2 gap-5">
                <div class="m-auto">
                    <h3 class="h3">All categories</h3>

                    <table class="text-center">
                        <tr>
                            <th>Category Title</th>
                            <th>Edit</th>
                        </tr>
                        <?php 
                        
                        foreach ($categories as $category) {
                            echo '
                            <tr>
                                <td>'.$category['category_title'].'</td>
                                <td><a href="edit_category.php?id='.$category['id'].'">Edit</a></td>
                            </tr>
                            ';
                        }
                        
                        ?>
                    </table>      
                    <div class="">
                        <a href="add_category.php"> + Add Category </a>
                    </div>
                </div>

                <div class="m-auto text-center">
                    <h3 class="h3">All Authors</h3>

                    <table class="text-center m-auto">
                        <tr>
                            <th>Author Name</th>
                            <th>Author Surname</th>
                            <th>Author's Biography</th>
                            <th>Edit</th>
                        </tr>
                        <?php 
                        
                        foreach ($authors as $author) {
                            echo '
                            <tr>
                                <td>'.$author['author_name'].'</td>
                                <td>'.$author['author_surname'].'</td>
                                <td>'.$author['biography'].'</td>
                                <td><a href="edit_author.php?id='.$author['id'].'">Edit</a></td>
                            </tr>
                            ';
                        }
                        
                        ?>
                    </table>
                    <div class="">
                        <a href="add_author.php"> + Add Author</a>
                    </div>
                </div>
            </div>

            <div class="grid grid-cols-2 gap-5">
                

            </div>
            
            
        </section>

        <section class="md:w-4/5 m-auto p-4 text-center">
            <h3 class="h3">Make new ADMIN</h3>
            <form action="" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password">
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" name="confirm_password" id="confirm_password">
                </div>
                <div class="form-group">
                    <label for="user_surname">Surname</label>
                    <input type="text" name="user_surname" id="user_surname">
                </div>
                <div class="form-group">
                    <label for="user_name">Name</label>
                    <input type="text" name="user_name" id="user_name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email">
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
                
            </form>
        </section>

        <section class="md:w-4/5 m-auto p-4 text-center">
            <h3 class="h3">All users</h3>

            <table class="text-center m-auto">
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Edit</th>
                </tr>
                <?php 
                
                foreach ($users as $user) {
                    echo '
                    <tr>
                        <td>'.$user['id'].'</td>
                        <td>'.$user['user_name'].'</td>
                        <td>'.$user['user_surname'].'</td>
                        <td>'.$user['email'].'</td>
                        <td>'.$user['user_role'].'</td>
                        <td><a href="edit_user.php?id='.$user['id'].'">Edit</a></td>
                    </tr>
                    ';
                }
                
                ?>
            </table>
            <div class="">
            </div>
        </section>

        <footer></footer>
        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>