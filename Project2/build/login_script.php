<?php 

require_once __DIR__ . '/Database/Connection.php';

use Database\Connection as Connection;

$connectionObj = new Connection();
$connection = $connectionObj->getPdo();

$email = $_POST['email'];
$password = $_POST['password'];

$data = [
    'email' => $email,
    'password' => $password
];

$sql = 'SELECT Users.*, Roles.* FROM Users INNER JOIN Roles on Users.role_id = Roles.id WHERE email = :email AND password = :password';
$stmt = $connection->prepare($sql);

$stmt -> execute($data);

$user = $stmt -> fetch(PDO::FETCH_ASSOC);
echo '<pre>';
print_r($user);
echo '</pre>';


if ($user) {
    if ($user['email'] == $email && $user['password'] == $password) {
        if ($user['user_role'] == 'Admin') {
            session_start();
            $_SESSION['id'] = $user['id'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['password'] = $user['password'];
            $_SESSION['user_name'] = $user['user_name'];
            $_SESSION['user_surname'] = $user['user_surname'];
            $_SESSION['role_id'] = $user['role_id'];
            // echo 'admin';
            // print_r($_SESSION);
            // echo '</pre>';
            // die();
            header("Location: admin_dashboard.php");
        } else {

            session_start();
            $_SESSION['id'] = $user['id'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['password'] = $user['password'];
            $_SESSION['user_name'] = $user['user_name'];
            $_SESSION['user_surname'] = $user['user_surname'];
            $_SESSION['role_id'] = $user['role_id'];
            echo 'client';
            print_r($_SESSION);
            echo '</pre>';
            header("Location: homepage.php?login=success");
        }
    }
} else {

    header("Location: login.php?error=Wrong email or password");
}
?>