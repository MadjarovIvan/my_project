<?php 



require_once __DIR__ . '/Database/Connection.php';
require_once __DIR__ . '/Categories/Categories.php';
require_once __DIR__ . '/Books/Books.php';


use Database\Connection as Connection;
use Categories\Categories as Categories;
use Books\Books as Books;

$connectionObj = new Connection();
$connection = $connectionObj->getPdo();

$categories_sql = 'SELECT * FROM Category';
$categories_stmt = $connection -> prepare($categories_sql);
$categories_stmt -> execute();

$books_sql = 'SELECT Books.*, authors.author_name, Category.category_title FROM Books INNER JOIN Authors on Books.author_id = Authors.id INNER JOIN Category on Books.category_id = Category.id';
$books_stmt = $connection -> prepare($books_sql);
$books_stmt -> execute();
?>


<!DOCTYPE html>
<html>
    <head>
        <title>BRAINSTER Library</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/a68a5718a6.js" crossorigin="anonymous"></script>
    </head>
    <body class="">

        <header class="min-w-full bg-black text-white">

            <nav class="md:w-4/5 m-auto flex justify-between p-4">
                <div class="">
                    <h1 class="">BRAINSTER Library</h1>
                </div>

                <div id="login_link" style="<?= isset($_GET['login']) ? 'display: none' : '' ?>" class="">
                    <a class="" href="login.php">Log in / Register</a>
                </div>
                <div id="logout_link" style="<?= isset($_GET['login']) ? 'display: block' : 'display: none' ?>" class="">
                    <a id="user_link" href="#">pic</a>
                    <a id="logout_link" href="">/ Log Out</a>
                </div>
            </nav>

        </header>
        
        <!-- //FILTERS -->
        <main class="min-h-screen">
            <section id="filter" class="bg-gray-700  text-white">
                <div class="md:w-4/5 m-auto p-1  flex justify-between">
                    <div class="flex items-center">
                        <span>filters:</span>
                        <span><?php 
                        
                        while ($rows = $categories_stmt->fetch()) {
                            
                            echo '
                            <input type="checkbox" name="category" id='. $rows['id'] .'>
                            <label for='. $rows['id'] .'>'. $rows['category_title'] .'</label>
                            ';

                        }
                        
                        ?></span>
                    </div>
                </div>
            </section>

            <!-- //BOOKS -->
            <section id="books">
                <div class="md:w-4/5 m-auto p-4 flex justify-between">
                    <div class="flex items-center">
                        <h2>Books</h2>
                        <?php 
                        
                        while ($rows = $books_stmt->fetch()) {
                            
                            echo '
                            <a href="">
                                <div class="book">
                                    <img src="images/'. $rows['book_image'] .'" alt="book image">
                                    <h3>'. $rows['book_title'] .'</h3>
                                    <p>'. $rows['author_name'] .'</p>
                                    <p>'. $rows['category_title'] .'</p>
                                </div>
                            </a>';
                        }
                        ?>
                    
                    </div>
                </div>
            </section>
        </main>

        <footer></footer>
        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>