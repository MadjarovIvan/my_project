<?php 
namespace Books;

require_once ('./Database/Connection.php');
use Database\Connection as Connection;
require_once ('./Authors/Authors.php');
use Author\Author as Author;
use Authors\Authors;

require_once ('./Categories/Categories.php');
use Categories\Categories as Categories;


class Books
{
    protected $id;
    protected $author_id;
    protected $book_title;
    protected $cathegory_id;
    protected $number_of_pages;
    protected $picture_url;
    protected $release_year;

    public function __construct($id, Authors $author, $book_title, $cathegory_id, $number_of_pages, $picture_url, $release_year)
    {
        $this->id = $id;
        $this->author_id = $author->getId();
        $this->book_title = $book_title;
        $this->cathegory_id = cathegory->$cathegory_id;
        $this->number_of_pages = $number_of_pages;
        $this->picture_url = $picture_url;
        $this->release_year = $release_year;
    }

    public function store() {

        $connectionObj = new Connection();
        $connection = ConnectionObj->getPdo();

        $data = [
            'author_id' => $this->author_id,
            'book_title' => $this->book_title,
            'cathegory_id' => $this->cathegory_id,
            'number_of_pages' => $this->number_of_pages,
            'picture_url' => $this->picture_url,
            'release_year' => $this->release_year
        ];
        
        $sql = "INSERT INTO books (author_id, book_title, cathegory_id, number_of_pages, picture_url, release_year) VALUES (:author_id, :book_title, :cathegory_id, :number_of_pages, :picture_url, :release_year)";
        $stmt = $connection->prepare($sql);
        $stmt->execute($data);

        $connectionObj->destroy();
    }
}

?>