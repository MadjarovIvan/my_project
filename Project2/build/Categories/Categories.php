<?php 
namespace Categories;

require_once ('./Database/Connection.php');
use Database\Connection as Connection;

class Categories
{
    protected $id;
    protected $category_title;

    public function getId() {
        return $this->id;
    }

    public function __construct($id, $category_title)
    {
        $this->id = $id;
        $this->category_title = $category_title;
    }

    public function store() {
        $connectionObj = new Connection();
        $connection = $connectionObj->getPdo();

        $data = [
            'category_title' => $this->category_title
        ];

        $sql = "INSERT INTO Category (category_title) VALUES (:category_title)";
        $stmt = $connection -> prepare($sql);
        $stmt = execute($data);

        $connectionObj->destroy();
    }
}


?>