<?php 
namespace Authors;

require_once ('./Database/Connection.php');
use Database\Connection as Connection;

class Authors
{
    protected $id;
    protected $author_name;
    protected $author_surname;
    protected $biography; 


    public function getId() {
        return $this->id;
    }
    
    public function __construct($id, $author_name, $author_surname, $biography)
    {
        $this->id = $id;
        $this->author_name = $author_name;
        $this->author_surname = $author_surname;
        $this->biography = $biography;
    }

    public function store() {
        $connectionObj = new Connection();
        $connection = $connectionObj->getPdo();

        $data = [
            'author_name' => $this->author_name,
            'author_surname' => $this->author_surname,
            'biography' => $this->biography
        ];

        $sql = "INSET INTO authors (author_name, author_surname,biography) VALUES (:author_name, :author_surname, :biography)";
        $stmt = $connection -> prepare($sql);
        $stmt = execute($data);

        $connectionObj->destroy();
    }
}
?>