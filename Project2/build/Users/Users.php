<?php 

namespace Users;

require_once __DIR__ . '/../Database/Connection.php';

use Database\Connection as Connection;

class Users
{
    protected $id;
    protected $user_name;
    protected $user_surname;
    protected $email;
    protected $password;

    public function setUserName($user_name) {
        $this->user_name = $user_name;
    }
    
    public function setUserSurname($user_surname) {
        $this->user_surname = $user_surname;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function store() {

        $connectionObj = new Connection();
        $connection = $connectionObj->getPdo();

        $data = [
            'user_name' => $this->user_name,
            'user_surname' => $this->user_surname,
            'email' => $this->email,
            'password' => $this->password
        ];

        
        $sql = "INSERT INTO users (user_name, user_surname, email, password) VALUES (:user_name, :user_surname, :email, :password)";
        $stmt = $connection->prepare($sql);
        $stmt->execute($data);

        $connectionObj->destroy();
    }
}




?>