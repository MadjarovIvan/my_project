<?php 
require_once __DIR__ . '/Database/Connection.php';
require_once __DIR__ . '/Books/Books.php';
require_once __DIR__ . '/Authors/Authors.php';
require_once __DIR__ . '/Categories/Categories.php';

use Database\Connection as Connection;
use Books\Books as Books;
use Authors\Authors as Authors;
use Cathegory\Cathegory as Cathegory;

$connectionObj = new Connection();
$connection = $connectionObj->getPdo();
// $mybooks = 'SELECT * FROM Books';
// $allbooks = $connection -> prepare($mybooks);
// $allbooks -> execute();
// $books = $allbooks -> fetchAll(PDO::FETCH_ASSOC);
// echo '<pre>';
// print_r($books);
// echo '</pre>';
// die();
$sql = 'SELECT Books.*, Authors.*, Category.category_title FROM Books LEFT JOIN Authors ON Books.author_id = Authors.id LEFT JOIN Category ON Books.category_id  = Category.id WHERE Books.id = 1';
$stmt = $connection -> prepare($sql);
$stmt -> execute();
$book = $stmt -> fetchAll();
echo '<pre>';
print_r($book);
echo '</pre>';

?>


<!DOCTYPE html>
<html>
    <head>
        <title>BRAINSTER Library</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/a68a5718a6.js" crossorigin="anonymous"></script>
    </head>
    <body class="min-h-screen">
        
        <h1><?php echo $book['author_name'] ?></h1>
        <div class="columns-2">
            <div class="w-full">
                <figure>
                    <img src="<?= $book['picture_url'] ?>" alt="BookCover <?= $book['book_title'],  $book['author_name']?>">
                </figure>
                <figcaption><?= $book['book_title'], $book[''] ?></figcaption>
                <div>
                    <h3>My Comments:</h3>
                    
                    <div>
                        <h4>Add new Comment</h4>
                        <form action="" method="post">
                            <textarea name="" id="" cols="30" rows="10"></textarea>
                            <button class="btn btn-primary">Add comment</button>    
                        </form>
                    </div>
                </div>
            </div>
            <div class="w-full">
                <h3></h3>
            </div>
        </div>

        <footer></footer>
        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>